import const
import io_utils
import convert_utils
import spikein_normalize
import basic_fit
import filter
import utils
import visualize

from collections import defaultdict
import sys

class Pipeline:
	def __init__(self):
		self.rpkms = []
		self.rpkms_d = defaultdict(list)
		self.rpkms_nrmz_d = defaultdict(list)
		self.rpkms_f_d = defaultdict(list)
		self.fit_scores_d= defaultdict(list)



	def load_input(self,infile, count_type):
		temp_list =[]
		if count_type == 'rpkm':
			temp_list = io_utils.read_input_t1(infile)
			self.set_rpkm_d(temp_list)
			return temp_list
			#return temp_list
			#return convert_utils.get_rc_from_rpkm(temp_list)
		elif count_type == 'rpm':
			temp_list = io_utils.read_input_t1(infile)
			return convert_utils.get_rc_from_rpm(temp_list)
		elif count_type ==  'raw':
			temp_list = io_utils.read_input_t1(infile)
			return temp_list
		elif count_type == 'n_lrpm':
			temp_list = io_utils.read_input_t1(infile)
			return convert_utils.get_rpkm_from_lrpm(temp_list)
		elif count_type == 'fitted_rpkm':
			temp_list = io_utils.read_input_t2(infile)
			self.set_rpkms_f_d(temp_list)
			self.set_fit_scores(temp_list)
			self.set_rpkm_d(temp_list)
			return temp_list

	def set_rpkms_nrmz_d(self, nrpkms):
		self.rpkms_nrmz_d = utils.countdict_from_list_eibased(nrpkms, False)

	def get_rpkms_nrmz_d(self):
		return self.rpkms_nrmz_d

	def set_rpkm_d(self, rpkms):
		self.rpkms_d = utils.countdict_from_list_eibased(rpkms, False)

	def get_rpkms_d(self):
		return self.rpkms_d

	def set_rpkms_f_d(self, rpkmsf):
		self.rpkms_f_d = utils.countdict_from_list_eibased(rpkmsf, True)

	def get_rpkms_f_d(self):
		return self.rpkms_f_d

	def set_fit_scores_d(self, rpkmsf):
		self.fit_scores_d = utils.scoredict_from_list_eibased(rpkmsf)

	def get_fit_scores_d(self):
		return self.fit_scores_d




def general_pipeline(infile, count_type, prefix):
	p = Pipeline()
	rcs = p.load_input(infile, count_type)
	## Normalization
	#n_lrpms = spikein_normalize.run_normalization_byR(rcs, prefix)
	#n_rpkms = convert_utils.get_rpkm_from_lrpm(n_lrpms)
	
	print "going to generate the ratios"
	n_rpkms = convert_utils.get_ratio_from_rpkms(rcs[:4374])
	print "Just generated the ratios"
	
	## Fitting
	black_list = filter.filter_by_thresh(n_rpkms, 0.1, 4, 1)

	print " there are   " + str(len(black_list)) + "  instants  in the blacklist"
	fitteds = basic_fit.fit_both(n_rpkms, black_list)

	print " there are   " + str(len(fitteds)) + "  instants fitted"
	after_fit_blacklist = filter.filter_after_fit(rcs, fitteds)

	print " there are   " + str(len(after_fit_blacklist)) + "  instants  in the blacklist"
	fitted_filtered = filter.filter_by_blacklist(fitteds, after_fit_blacklist)
	io_utils.save_fitted(fitted_filtered, prefix)

	print "Now I need to make some dictionaries, will take a while."
	p.set_rpkms_f_d(fitted_filtered)
	#p.set_rpkms_nrmz_d(n_rpkms)
	p.set_fit_scores_d(fitted_filtered)

	p.set_rpkm_d(n_rpkms)
	
	##Visualization
	fitteds_dict = p.get_rpkms_f_d()
	rpkms_dict = p.get_rpkms_d()
	#nrpkms_dict = p.get_rpkms_nrmz_d()
	scores_dict = p.get_fit_scores_d()

	popt_dict = utils.poptdict_from_list_eibased(fitted_filtered)

	print len(fitteds_dict), len(rpkms_dict)
	#len(nrpkms_dict)
	print len(scores_dict)

	for seg in fitteds_dict:
		print seg
		#print fitteds_dict[seg]
		visualize.plot_raw_nrm_fit(rpkms_dict[seg][0], rpkms_dict[seg][0], fitteds_dict[seg][0],
								   seg, scores_dict[seg][0], popt_dict[seg])

#general_pipeline(sys.argv[1], sys.argv[2], sys.argv[3])


