'''
This module includes configurable constants
'''

import numpy as np

'''
Time points
'''

TP = np.array([0,1,2,5,10,15,30,60])
TP_FIT = np.linspace(0, 60, 100)

'''
Input File
'''
from collections import namedtuple

DP = namedtuple('DP', 'name chrm strand start end length counts EI')



'''
LIB SIZE
'''
LIB_SIZE = [14137233, 12231890, 12434641, 19677555, 16532473, 17422464, 17761308, 14987905]
ACT_SIZE = [38618, 22164, 39970, 51709, 11832, 15654, 4548, 2447]


'''
Output File
'''
DP_FIT = namedtuple('DP_FIT', 'name chrm strand start end length counts EI fitted popt error error_n error_d')
F_DP_FIT = namedtuple('F_DP_FIT', 'name chrm strand start end length counts EI fitted popt error error_n error_d snr rsquared pearson tli fit_type')

'''
FIT
'''

D_INIT = [0.002, 0.1, 0.1]
A_INIT = [1, 0.1]
