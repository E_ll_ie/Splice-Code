import const
import io_utils
import convert_utils
from matplotlib import pyplot as plt
import math

def plot(exprs, fits, pname):
	plt.plot(const.TP, exprs, 'o', label='data')
	plt.plot(const.TP_FIT, fits, 'b-', label='fit')
	plt.legend()
	plt.savefig('images-test/' + str(pname) + ".png")
	plt.close()


def plot_raw_nrm_fit(direct, raw, nrm, fit, pname, scores, popt):
	'''
	plot raw, normalized and fit data. Show the scores in the plot
	:param raw: observed rpkms
	:param nrm: normalized rpkms
	:param fit: fitted rpkms
	:param pname: string :name of exon
	:param scores: list : all scores
	:return:
	'''
	#print raw
	#print nrm
	#print fit
	#print len(const.TP_FIT)
	#print len(fit)
	plt.figure(figsize=(18, 14))
	plt.plot(const.TP, raw, 'o', color = 'k' , label = 'rpkms', markersize = 6)
	#plt.plot(const.TP, nrm, '^', color = 'g', label = 'normalized rpkms')
	plt.plot(const.TP_FIT, fit[:100], '-', color = 'k', label = 'fit' , markersize=6)
	plt.plot(const.TP_FIT, fit[100:200], '--', color = 'k', label = 'confidence-interval')
	plt.plot(const.TP_FIT, fit[200:300], '--', color = 'k', label = 'confidence-interval')
	plt.fill_between(const.TP_FIT, fit[100:200], fit[200:300], facecolor = 'k', alpha = 0.2)
	plt.ylabel('Expression(RPKM)')
	plt.xlabel('Time(Min)')
	temp = "MSE:" + str(scores[0]) + " SNR:" + str(scores[1]) +\
		   " R2:" + str(scores[2][0]) + "," + str(scores[2][1]) + " Pearson:" + str(scores[3]) + " TLI:" + str(scores[4])
	plt.title(temp)
	plt.legend()
	#plt.show()
	#plt.figure(figsize=(14, 10))
	plt.savefig(direct + '/' + str(pname).replace("/" , "-") + ".png")

def plot_pca():
	##TODO get the counts in each step
	## More useful when replicates are available
	return




def plot_all_in_gene(gene_list):
	##TODO get the gene name, plot all fitted curved with their error
	## Some have too many? Think about it later.
	for tps in gene_list:
		leg_txt = tps[0] + "-" + str(tps[1]) + "-" + str(tps[2]) + "-" + str(tps[-4]) + "-" + tps[-1]
		plt.plot(const.TP_FIT, tps[8], label = leg_txt)
	plt.legend()
	plt.savefig(str(tps[0])+"-allparts.png")


def run_visualize(args):
	fitted = io_utils.read_input_t2(args.input)
	if args.gene_list == 'all':
		for dp in fitted:
			seg = dp[0]+ "=" + dp[2]+ "=" + str(dp[3])
			#print fitteds_dict[seg]
			plot_raw_nrm_fit(args.output_folder, dp[6], dp[6], dp[8],
						 	seg, [dp[10], dp[13], dp[14], dp[15], dp[16]], dp[9])

	else:
		gl = open(args.gene_list, 'r').read().splitlines()
		for dp in fitted:
				if dp[0] in gl:
					seg = dp[0] + "=" + dp[2] + "=" + str(dp[3])
					# print fitteds_dict[seg]
					plot_raw_nrm_fit(args.output_folder, dp[6], dp[6], dp[8],
									 seg, [dp[10], dp[13], dp[14], dp[15], dp[16]], dp[9])
