import sys
import io_utils
from matplotlib import pyplot as plt

def run_stats(args):
	fitted = io_utils.read_input_t2(args.input)
	m1list = []
	m2list = []
	m3list = []
	direct = args.output_folder
	for dp in fitted:
		popts = dp[9]
		if dp[-1] == 'aprc':
			m1list.append(popts[0])
			m2list.append(popts[1])
		if dp[-1] == 'dprc':
			m1list.append(popts[0])
			m2list.append(popts[1])
			m3list.append(popts[2])

	print "===================================================="
	print "Fitting Function is " + dp[-1]
	if len(m1list) > 0 :
		plt.figure()
		plt.hist(m1list)
		plt.savefig(direct + '/m1.png')
	if len(m2list) > 0:
		plt.figure()
		plt.hist(m2list)
		plt.savefig(direct + '/m2.png')
	if len(m3list) > 0:
		plt.figure()
		plt.hist(m3list)
		plt.savefig(direct + '/m3.png')