import const
import sys


def read_input_t1(infile, skipfirst = 1):
	'''
	Read the initial input file and similar files
	Check DP type for the details
	:param infile: list of DP
	:return:
	'''
	exp_file = open(infile, 'r').read().splitlines()
	list_of_dp = []
	for line in exp_file[skipfirst:]:
		fds = line.split()
		dp = const.DP(fds[0], fds[1], fds[2], int(fds[3]), int(fds[4]), int(fds[5]), map(float, fds[6:14]), fds[-1])
		list_of_dp.append(dp)

	return list_of_dp


def read_input_t2(infile):
	'''
	Read the file saved after fitting, so it includes all different information
	About fitting values, errors and scores
	Check the structure of F_DP_FIT fo the details
	:param infile:
	:return:
	'''
	fit_file = open(infile, 'r').read().splitlines()
	list_of_f_dp = []
	count = 0
	for line in fit_file[0:]:
		count += 1
		fds = line.split("\t")
		scores = (fds[17].replace('(', '')).replace(')','').split(";")
		if count % 50000 == 0:
			print str(count) + "   are imported."
		for i in range(8):
			if scores[i] != "None":
				if i!=6:
					scores[i] = float(scores[i])
				else:
					scores[i] = [float(scores[i].split(',')[0]), float(scores[i].split(',')[1])]

		f_dp = const.F_DP_FIT(fds[0], fds[1], fds[2], fds[3], fds[4], fds[5], map(float, fds[6:14]), fds[14], \
							map(float, fds[15].split(',')), map(float, fds[16].split(";")), scores[0], \
							scores[1], scores[2] , scores[3],
							(scores[4], scores[5]), scores[6][0],
							scores[7], fds[18])
		list_of_f_dp.append(f_dp)
	return list_of_f_dp


def save_fitted(list_of_f_dp, prefix):
	'''
	Save a list of F_DP_FIT s
	:param list_of_f_dp:
	:param prefix:
	:return:
	'''
	outfile = open(prefix + "_fitted-values.txt", 'w')
	for fds in list_of_f_dp:
		#print fds
		temp = "\t".join(map(str, fds[0:6])) + "\t" + "\t".join(map(str,fds[6])) + "\t" + fds[7] + "\t" + ",".join(
			map(str,fds[8])) + "\t" + ";".join(map(str,fds[9])) \
			   + "\t" + ";".join(map(str,fds[10:14]))+ ";" + str(fds[14][0])+ ";" + str(fds[14][1]) + ";" + str(fds[15]) + ";" + str(fds[16]) + "\t" + fds[17]
		outfile.write(temp + "\n")
	outfile.close()


def load_fitted(infile):
	fitted_f = open(infile, 'r').read().splitlines()
	##TODO complete

def save_normalized(rpkms, tempforsave):
	temp_out_for_save = open(tempforsave, 'w')
	for fds in rpkms:
		temp_out_for_save.write("\t".join(fds[0:6]) + "\t" + "\t".join(fds[6]) + "\t" + fds[-1] + "\n")
	temp_out_for_save.close()


def load_normalized(tempforload):
	temp_in_from_load = open(tempforload, 'r').read().splitlines()
	temp_rpkms = []
	for line in temp_in_from_load:
		fds = line.split()
		dp = const.DP(fds[0], fds[1], fds[2], int(fds[3]), int(fds[4]), int(fds[5]), map(float, fds[6:14]), fds[-1])
		temp_rpkms.append(dp)

	return temp_rpkms


def save_rcs_for_Rnormalization(rcs, tempforr):
	temp_out_for_r = open(tempforr, 'w')
	for fds in rcs:
		temp_out_for_r.write("=".join(map(str,fds[0:6])) + "=" + fds[-1] + "\t" + "\t".join(map(str,fds[6])) + "\n")
	temp_out_for_r.close()


def load_lrpms_after_Rnormalization(tempfromr):
	'''
	Reads the normalized numbers from a file written by the R script
	Put them in variable for now
	:param tempfromr: normalized file from r
	:return:
	'''
	##TODO do I need to keep the result of this middle step?
	temp_lrpms = []
	print "Reading R normalized values ...." 
	temp_in_from_r = open(tempfromr, 'r').read().splitlines()
	for line in temp_in_from_r[1:]:
		fds = line.split()
		fes = fds[0].split("=")
		#print fes
		dp = const.DP(fes[0][1:], fes[1], fes[2], int(fes[3]), int(fes[4]), int(fes[5]), map(float, fds[1:9]), fes[-1][0])
		temp_lrpms.append(dp)
	return temp_lrpms
	#self._change_to_rpkm_from_lrpm()

def save_ratios_from_rpkms(tempratio, ratiofile):
	temp_out_for_r = open(ratiofile, 'w')
	for fds in tempratio:
		temp = "\t".join(map(str, fds[0:6])) + "\t" + "\t".join(map(str, fds[6])) + "\t" + fds[7] + "\n"
		temp_out_for_r.write(temp)

	temp_out_for_r.close()
