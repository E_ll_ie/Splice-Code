import numpy as np
from collections import namedtuple
import const
import math
from scipy.stats import pearsonr


def report_simple_snr(y, y_fit):
	sigma_y = np.var(np.array(y))
	sigma_yfit = np.var(np.array([abs(i - j) for i, j in zip(y, y_fit)]))
	#print sigma_y, sigma_yfit
	if sigma_y==0:
		sigma_y=0.00000001
	return math.log((sigma_y/sigma_yfit),10)


def report_pearson_corr(y, y_fit):
	return pearsonr(y, y_fit)


def report_r_squared(y, y_fit, num_p):

	residuals = np.array([abs(i - j) for i, j in zip(y, y_fit)])
	ss_res = np.sum(residuals ** 2)
	ss_tot = np.sum((y - np.mean(y)) ** 2)
	r_squared = 1 - (ss_res / ss_tot)
	adj_r_squared = 1 - (((1 - r_squared)*(len(const.TP) - 1))/(len(const.TP) - num_p ))
	
	return (r_squared, adj_r_squared)


def report_TLI(y, y_fit, num_p):
	#TODO Make sure it is useful in our case
	'''
	:param y:
	:param y_fit:
	:param num_p:
	:return:
	'''
	df_model =  len(const.TP) - num_p
	df_null = len(const.TP)
	y = np.array(y)
	x_null = np.sum( (y - np.mean(y)) ** 2)
	residuals = np.array([abs(i - j) for i, j in zip(y, y_fit)])
	x_model = np.sum(residuals ** 2)
	score = ((x_null/df_null) - (x_model/df_model))/((x_null/df_null) - 1)
	return score



