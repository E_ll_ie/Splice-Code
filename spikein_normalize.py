import const
import io_utils
import convert_utils
import filter
import subprocess
import pipeline

def run_normalization_byR(rcs, prefix):
	##TODO work on this
	'''
	load R and normalize
	:return:
	'''
	print "make raw count file ready for R ..."
	io_utils.save_rcs_for_Rnormalization(rcs, prefix + "_temp-for-r.txt")
	loadcode = subprocess.call(['module load R/3.1.0'], shell=True)
	##TODO I need to specify inpur argument for the R script used for normalization.
	##TODO Maybe the type of normalization, and prefix of output files.
	retcode = subprocess.call(['Rscript /cbcl/forouzme/Projects/SpliceCode/scripts/normalize-by-loess.r ' +  prefix + "_temp-for-r.txt " + prefix], shell=True)
	print "Read count are normalized by R using spike in data \n"
	return io_utils.load_lrpms_after_Rnormalization(prefix + "_normalized-by-r-ercc.txt")

def run_normalize_spikein(args):
	p = pipeline.Pipeline()
	rcs = p.load_input(args.input, args.input_type)
	## Normalization
	n_lrpms = run_normalization_byR(rcs, args.output)
	n_rpkms = convert_utils.get_rpkm_from_lrpm(n_lrpms)