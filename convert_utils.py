import const
import math
import io_utils
import pipeline


def get_rpm_from_rpkm(rpkms):
	temp_rpms =[]
	for fds in rpkms:
		rpms = [e * fds[5]/1000 for e in fds[6]]
		dp = const.DP(fds[0], fds[1], fds[2], int(fds[3]), int(fds[4]), int(fds[5]), rpms, fds[-1])
		temp_rpms.append(dp)

	return temp_rpms


def get_rc_from_rpm(rpms):
	temp_rcs =[]
	for fds in rpms:
		rcs = []
		for i in range(len(fds[6])):
			rcs.append(int(round((fds[6][i] * const.LIB_SIZE[i]) / (math.pow(10, 6)))))
		dp = const.DP(fds[0], fds[1], fds[2], int(fds[3]), int(fds[4]), int(fds[5]), rcs, fds[-1])
		temp_rcs.append(dp)
	return temp_rcs


def get_rc_from_rpkm(rpkms):
	temp_rcs =[]
	for fds in rpkms:
		rcs = []
		for i in range(len(fds[6])):
			rcs.append(int(round((fds[6][i] * float(fds[5]) * const.LIB_SIZE[i]) / (math.pow(10, 9)))))

		dp = const.DP(fds[0], fds[1], fds[2], int(fds[3]), int(fds[4]), int(fds[5]), rcs, fds[-1])
		temp_rcs.append(dp)
	return temp_rcs


def get_rpkm_from_lrpm(lrpms):
	'''
	From normalized number back to rpkm
	:return:
	'''
	print "Going back to rpkms from log of count per million imported from R"
	temp_rpkms =[]
	for fds in lrpms:
		rpkms = []
		for i in range(len(fds[6])):
			rpkms.append(1000 * (math.pow(2, fds[6][i])) / float(fds[5]))
			#rpkms.append((1000 * fds[6][i]) / float(fds[5]))
		dp = const.DP(fds[0], fds[1], fds[2], int(fds[3]), int(fds[4]), int(fds[5]), rpkms, fds[-1])
		temp_rpkms.append(dp)
	return temp_rpkms

def get_ratio_from_exons_introns(exons, introns):
	all =[]
	for i in range(len(introns)):
		ratios = []
		if len(exons) >= i+2:
			for j in range(len(const.TP)):
				ratios.append((exons[i][6][j]+ exons[i+1][6][j])/2)
			fds = introns[i]
			temp_dp = const.DP(fds[0], fds[1], fds[2], fds[3], fds[4], fds[5],  ratios, fds[-1])
			all.append(temp_dp)

	for i in range(1,len(exons)-1):
		ratios = []
		for j in range(len(const.TP)):
			ratios.append((exons[i-1][6][j]+ exons[i+1][6][j])/2)
		fds = exons[i]
		temp_dp = const.DP(fds[0], fds[1], fds[2], fds[3], fds[4], fds[5], ratios, fds[-1])
		all.append(temp_dp)
	return all

def get_ratio_from_rpkms(rpkms):
	print len(rpkms)
	ratio_list =[]
	temp_name =  rpkms[0][0]
	intron_list = []
	exon_list = []
	count = 0
	for dp in rpkms:
		count +=1
		#print dp
		if dp[0] == temp_name:
			if dp[-1] == "E":
				exon_list.append(dp)
			elif dp[-1]=="I":
				intron_list.append(dp)
		else:
			ratio_list += get_ratio_from_exons_introns(exon_list, intron_list)
			temp_name = dp[0]
			exon_list = []
			intron_list = []
			if dp[-1] == "E":
				exon_list.append(dp)
			elif dp[-1]=="I":
				intron_list.append(dp)

		if count%50000 == 0 :
			print str(count) + "   ratio done"
	ratio_list += get_ratio_from_exons_introns(exon_list, intron_list)

	return ratio_list




def run_converttoratio(args):
	print args
	p = pipeline.Pipeline()
	rcs = p.load_input(args.input, 'rpkm')
	print "We're going to generate the ratios in this step ..."
	print "===================================================="
	r_rpkms = get_ratio_from_rpkms(rcs[:])
	io_utils.save_ratios_from_rpkms(r_rpkms, args.output)
	print "Just generated the ratios."
	print "the output is saved in:  " + str(args.output) + "\n"
	print "===================================================="




