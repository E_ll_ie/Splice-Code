import argparse
import const
import io_utils
from convert_utils import run_converttoratio
from spikein_normalize import run_normalize_spikein
from basic_fit import run_basic_fit
from filter import run_filter_after_fit
from filter import run_filter_before_fit

import utils
from visualize import run_visualize
from spstats import run_stats

from collections import defaultdict
import sys

parser = argparse.ArgumentParser(description='Lets model the expression time series')
subparsers = parser.add_subparsers()

#============================================
# ConvertToRatio Subcommands
#============================================

parser_ConvertToRatio = subparsers.add_parser('ConvertToRatio',
                                        help='Convert the rpkms to the ratio, '
											 'done for middle exons and introns, '
											 'based on previous and next exons')

parser_ConvertToRatio.add_argument('--input',
								   help = 'This is the input file,'
										  'For now the default is rpkm')
parser_ConvertToRatio.add_argument('--input_type',
								   help = 'Choose between raws(read counts), rpkms, rpms')

parser_ConvertToRatio.add_argument('--output',
								   help = 'This is the output file')

parser_ConvertToRatio.set_defaults(func=run_converttoratio)

#============================================
# NormalizeBySpikein Subcommands
#============================================

parser_NormalizeBySpikeins = subparsers.add_parser('NormalizeBySpikeins',
                                        help= 'In case you have spike in data, '
											  'this module can be used to Normalize')
parser_NormalizeBySpikeins.add_argument('--input',
								   help = 'This is the input file')

parser_NormalizeBySpikeins.add_argument('--input_type',
								   help = 'Choose between raws(read counts), rpkms, rpms')

parser_NormalizeBySpikeins.add_argument('--output',
								   help = 'This is the output file')

parser_NormalizeBySpikeins.set_defaults(func=run_normalize_spikein)

#============================================
# Fit Subcommands
#============================================
parser_Fit = subparsers.add_parser('Fit',
								   help = 'This module used simple fitting functions')


parser_Fit.add_argument('--input', help = 'This is the input file,'
										  'For now the default is rpkm')

parser_Fit.add_argument('--fitting_function', help = 'You can choose between different predefined functions, '
													'choose between aprc, dprc, or best')

parser_Fit.add_argument('--output',
						help='This is the output file containing values after fitting')
parser_Fit.set_defaults(func=run_basic_fit)

#============================================
# Display Subcommands
#============================================
parser_Display = subparsers.add_parser('Display',
								   help = 'This module used simple fitting functions')


parser_Display.add_argument('--input', help = 'This is the input file (fitted),'
										  'For now the default is rpkm')


parser_Display.add_argument('--gene_list', help = 'list of genes in a file or you can just say all')

parser_Display.add_argument('--output_folder', help = 'Directory to be used for saving images')

parser_Display.set_defaults(func=run_visualize)


#============================================
# GetStats Subcommands
#============================================
parser_GetStats = subparsers.add_parser('GetStats',
								   help = 'This module used simple fitting functions')


parser_GetStats.add_argument('--input', help = 'This is the input file with values after fitting,'
										  'The fitting args should be present there')

parser_GetStats.add_argument('--output-folder', help = 'Some plots will be saved in this directory')

parser_GetStats.set_defaults(func=run_stats)


#============================================
# FilterByScores Subcommands
#============================================
parser_FilterByScores = subparsers.add_parser('FilterByScores',
								   help = 'This module used simple fitting functions')


parser_FilterByScores.add_argument('--input', help = 'This is the input file with values after fitting,'
										  'The fitting args should be present there')

parser_FilterByScores.add_argument('--filter_type', help = 'choose between snr, tli, mse, r2, r2adj, pearson')
parser_FilterByScores.add_argument('--threshold', type=float, help = 'Choose your threshold based on the criteria')


parser_FilterByScores.add_argument('--output', help = 'Filtered and fitted output')

parser_FilterByScores.set_defaults(func=run_filter_after_fit)



#============================================
# FilterByExpr Subcommands
#============================================
parser_FilterByExpr = subparsers.add_parser('FilterByExpr',
								   help = 'This module used simple fitting functions')


parser_FilterByExpr.add_argument('--input', help = 'This is the input file with values after fitting,'
										  'The fitting args should be present there')


parser_FilterByExpr.add_argument('--output', help = 'Filtered and fitted output')

parser_FilterByExpr.set_defaults(func=run_filter_before_fit)


#======================================
# run
#======================================
if __name__ == '__main__':
    args = parser.parse_args()
    args.func(args)