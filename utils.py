import numpy as np
from collections import namedtuple
import const
from collections import defaultdict

def nrmz(thislist):
	if max(thislist) != 0 and max(thislist)-min(thislist) != 0:
		thislist_n = [1*(e-min(thislist))/(max(thislist)-min(thislist)) for e in thislist]
	elif max(thislist) != 0 and max(thislist)-min(thislist) == 0:
		thislist_n = [1 for e in thislist]
	else:
		thislist_n = thislist
	return thislist_n


def divide_by_max(thislist):
	'''
	:param thislist: is a list of number
	:return: the list of number divided by max if maximum is not zero
	'''

	if max(thislist)!=0:
		thislist_n = [e/max(thislist) for e in thislist]
	else:
		thislist_n = thislist
	return thislist_n


def countdict_from_list_gbased(counts, fitted):
	'''
	This makes  a dictionary in which keys are the gene names
	and the values are counts.
	Tf fitted values are both raw counts and fitted counts.
	:param counts: a list of entries of DP_FIT or F_DP_FIT type
	:param fitted: boolean to show if it is already fitted or not
	:return: a new dictionary
	'''
	temp_dict = defaultdict(list)
	for fds in counts:
		if not fitted:
			temp_dict[fds[0]].append(fds[6])
		else:
			temp_dict[fds[0]].append((fds[6],fds[8]))
	return temp_dict


def countdict_from_list_eibased(counts, fitted):
	'''
	Similar to previous function but this one is exon based
	:param counts:
	:param fitted:
	:return:
	'''
	temp_dict = defaultdict(list)
	for fds in counts:
		if not fitted:
			temp_dict[fds[0]+ "=" + fds[2]+ "=" + str(fds[3])].append(fds[6])
		else:
			temp_dict[fds[0]+ "=" + fds[2]+ "=" + str(fds[3])].append(fds[8])
	return temp_dict


def scoredict_from_list_eibased(counts):
	'''
	generates a dict with exon as keys and scores as values
	:param counts: a list of F_DP_FIT
	:return:
	'''
	temp_dict = defaultdict(list)
	for fds in counts:
		scores = [fds[10], fds[13], fds[14], fds[15], fds[16]]
		temp_dict[fds[0]+ "=" + fds[2]+ "=" + str(fds[3])].append(scores)
	return temp_dict



def poptdict_from_list_eibased(counts):
	'''
	generates a dict with exon as keys and scores as values
	:param counts: a list of F_DP_FIT
	:return:
	'''
	temp_dict = defaultdict(list)
	for fds in counts:
		scores = fds[9]
		temp_dict[fds[0]+ "=" + fds[2]+ "=" + str(fds[3])].append(scores)
	return temp_dict



def find_gene(g_f_rpkms_dict, gname):
	##TODO Do the fitting and normalization, then just show this
	##Easier if we can load fitted data for sure
	return g_f_rpkms_dict[gname]

def find_ex_or_int(e_i_f_rpkms_dict, gname, start, end):
	##Same as previous one
	return e_i_f_rpkms_dict[gname + "=" + str(start) + "=" + str(end)]




