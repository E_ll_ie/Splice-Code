import sys
import numpy as np
from scipy.optimize import curve_fit
from scipy.stats.distributions import  t
import random
import utils
import matplotlib.pyplot as plt
import const
from collections import defaultdict
import math
import subprocess
import io_utils
import convert_utils
import fit_evaluation
import pipeline
import filter

random.seed(7)

def fit_dprc_func(x, m1, m2, m3):
	'''
	modeling functions 1
	:param x: time points
	:param m1:
	:param m2:
	:param m3:
	:return:
	'''
	return (m1/(m2-m1))*((np.exp(-m1*x))-(np.exp(-m2*x)))+m3

def fit_aprc_func(x, m1, m2):
	'''
	modeling functions 2
	:param x:
	:param m1:
	:param m2:
	:return:
	'''
	return m1 * (1 - np.exp(-m2 * x))

def get_conf_interval_dprc(p, covp , y):
	upperband = []
	lowerband = []
	a = p[0]
	b = p[1]
	c = p[2]
	dof = len(const.TP) - len(p)
	alpha = 0.05
	prb = 1.0 - alpha / 2
	tval = t.ppf(prb, dof)
	n = len(p)
	C = covp

	for i in range(len(const.TP_FIT)):
		x = const.TP_FIT[i]
		dfdp = [(a*(np.exp(-a*x) - np.exp(-b*x)))/(a - b)**2 - (np.exp(-a*x) - np.exp(-b*x))/(a - b) + (a*x*np.exp(-a*x))/(a - b)			, -(a * (np.exp(-a * x) - np.exp(-b * x))) / (a - b) ** 2 - (a * x * np.exp(-b * x)) / (a - b) , 1]
		df2 = 0
		for j in range(n):
			for k in range(n):
				df2 += dfdp[j] * dfdp[k] * C[j, k]
		df = np.sqrt(df2)

		delta = tval * df
		upperband.append(y[i] + delta)
		lowerband.append(y[i] - delta)

	return (np.array(upperband), np.array(lowerband))


def get_conf_interval_aprc(p, covp, y):
	upperband = []
	lowerband = []
	a = p[0]
	b = p[1]
	dof = len(const.TP) - len(p)
	alpha = 0.05
	prb = 1.0 - alpha / 2
	tval = t.ppf(prb, dof)
	n = len(p)
	C = covp

	for i in range(len(const.TP_FIT)):
		x = const.TP_FIT[i]
		dfdp = [1 - np.exp(-b * x),
				a * x * np.exp(-b * x)]
		df2 = 0
		for j in range(n):
			for k in range(n):
				df2 += dfdp[j] * dfdp[k] * C[j, k]
		df = np.sqrt(df2)

		delta = tval * df
		upperband.append(y[i] + delta)
		lowerband.append(y[i] - delta)

	return (np.array(upperband), np.array(lowerband))

def fit_dprc(inits, exprs, nrmz_z_o, nrmz_by_max ):
	'''
	fir based on the first equation
	:param inits: initial parameters
	:param exps: ydata, rpkms, rpms
	:return:
	'''
	ydata_n = utils.nrmz(exprs)
	ydata_d = utils.divide_by_max(exprs)
	try:
		popt, pcov = curve_fit(fit_dprc_func, const.TP, exprs, p0=inits)
		#sigma = [math.sqrt(pcov[0][0]),
		#		 math.sqrt(pcov[1][1]),
		#		 math.sqrt(pcov[2][2])]
		#print sigma
		fitted_func = fit_dprc_func(const.TP, popt[0], popt[1], popt[2])
		yfit = fit_dprc_func(const.TP_FIT, popt[0], popt[1], popt[2])
		#yfit_up = fit_dprc_func(const.TP_FIT, popt[0]+2*sigma[0], popt[1]+2*sigma[1], popt[2]+2*sigma[2])
		#yfit_down = fit_dprc_func(const.TP_FIT, popt[0]-2*sigma[0], popt[1]-2*sigma[1], popt[2]-2*sigma[2])
		yfit_up, yfit_down = get_conf_interval_dprc(popt, pcov, yfit)

		yfit_temp =np.concatenate( (yfit_up, yfit_down), axis=0)
		yfit = np.concatenate((yfit, yfit_temp), axis=0)
		error = ((np.array(fitted_func) - np.array(exprs))**2).sum()
		error_n = None
		error_d = None
		if nrmz_z_o:
			fitted_func_n = utils.nrmz(fitted_func)
			error_n = ((np.array(fitted_func_n) - np.array(ydata_n)) ** 2).sum()
		if nrmz_by_max:
			fitted_func_d = utils.divide_by_max(fitted_func)
			error_d = ((np.array(fitted_func_d) - np.array(ydata_d)) ** 2).sum()
	except RuntimeError:
		error = float("nan")
		error_n = None
		error_d = None
		popt = None
		yfit = None
		fitted_func = None
	return (fitted_func,yfit, popt, error, error_n, error_d)


def fit_aprc(inits, exprs, nrmz_z_o, nrmz_by_max):
	'''
	fit based on the second equation
	:param inits: initial parameters m1 m2
	:param exps: coverages, y data
	:return:
	'''
	ydata_n = utils.nrmz(exprs)
	ydata_d = utils.divide_by_max(exprs)
	try:
		popt, pcov = curve_fit(fit_aprc_func, const.TP, exprs, p0=inits)

		#sigma = [math.sqrt(pcov[0][0]),
		#		 math.sqrt(pcov[1][1])]
		fitted_func = fit_aprc_func(const.TP, popt[0], popt[1])
		yfit = fit_aprc_func(const.TP_FIT, popt[0], popt[1])
		#yfit_up = fit_aprc_func(const.TP_FIT, popt[0]-2*sigma[0], popt[1]-2*sigma[1])
		#yfit_down = fit_aprc_func(const.TP_FIT, popt[0]+2*sigma[0], popt[1]+2*sigma[1])
		yfit_up, yfit_down = get_conf_interval_aprc(popt, pcov, yfit)

		#print yfit
		#print yfit_down 
		yfit_temp =np.concatenate( (yfit_up, yfit_down), axis=0)
		yfit = np.concatenate((yfit, yfit_temp), axis=0)

		error = ((np.array(fitted_func) - np.array(exprs)) ** 2).sum()
		error_n = None
		error_d = None
		if nrmz_z_o:
			fitted_func_n = utils.nrmz(fitted_func)
			error_n = ((np.array(fitted_func_n) - np.array(ydata_n)) ** 2).sum()
		if nrmz_by_max:
			fitted_func_d = utils.divide_by_max(fitted_func)
			error_d = ((np.array(fitted_func_d) - np.array(ydata_d)) ** 2).sum()
	except RuntimeError:
		error = float("nan")
		error_n = None
		error_d = None
		popt = None
		yfit = None
		fitted_func = None

	return (fitted_func, yfit, popt, error, error_n, error_d)

def fit_both(n_rpkms, blacklist):
	#TODO  What if we want to work with the ratios
	#TODO  What if we dont want to choose
	#TODO Make three list, first equation, second, the best
	#TODO Whats a cleaner way to do this? Panda library maybe?
	'''
	Fit based on both of the functions, keep the best one based on error rate
	:param n_rpkms: normalized rpkms are the input (Using Spike In Data)
	:param blacklist:
	:return:
	'''
	temp_fitteds =[]
	for fds in n_rpkms:
		if fds[0] not in blacklist:
			#print "\t".join(map(str,fds[:4])) +"\n"
			a_fitted_func, a_yfit, a_popt, a_error, a_error_n, a_error_d = fit_aprc(const.A_INIT, fds[6], False, False)
			a_dp_fit = const.DP_FIT(fds[0], fds[1], fds[2], fds[3], fds[4], fds[5], fds[6], fds[7], a_yfit, a_popt, a_error, a_error_n, a_error_d)

			d_fitted_func, d_yfit, d_popt, d_error, d_error_n, d_error_d = fit_dprc(const.D_INIT, fds[6], False, False)
			d_dp_fit = const.DP_FIT(fds[0], fds[1], fds[2], fds[3], fds[4], fds[5], fds[6], fds[7], d_yfit, d_popt, d_error,
							d_error_n, d_error_d)
			if not math.isnan(a_error) and not math.isnan(d_error):
				if a_error < d_error:
					final_dp_fit = const.F_DP_FIT(fds[0], fds[1], fds[2], fds[3], fds[4], fds[5], fds[6], fds[7], a_yfit,
							a_popt, a_error, a_error_n, a_error_d, fit_evaluation.report_simple_snr(fds[6], a_fitted_func),
					        fit_evaluation.report_r_squared(fds[6], a_fitted_func, 2), fit_evaluation.report_pearson_corr(fds[6], a_fitted_func),
							fit_evaluation.report_TLI(fds[6], a_fitted_func, 2), "aprc")

				elif a_error >= d_error:
					final_dp_fit = const.F_DP_FIT(fds[0], fds[1], fds[2], fds[3], fds[4], fds[5], fds[6], fds[7], d_yfit,
							d_popt, d_error, d_error_n, d_error_d, fit_evaluation.report_simple_snr(fds[6], d_fitted_func),
					        fit_evaluation.report_r_squared(fds[6], d_fitted_func, 3), fit_evaluation.report_pearson_corr(fds[6], d_fitted_func),
							fit_evaluation.report_TLI(fds[6], d_fitted_func, 3), "dprc")
			elif not math.isnan(a_error) and math.isnan(d_error):
				final_dp_fit = const.F_DP_FIT(fds[0], fds[1], fds[2], fds[3], fds[4], fds[5], fds[6], fds[7], a_yfit,
							a_popt, a_error, a_error_n, a_error_d, fit_evaluation.report_simple_snr(fds[6], a_fitted_func),
					        fit_evaluation.report_r_squared(fds[6], a_fitted_func, 2), fit_evaluation.report_pearson_corr(fds[6], a_fitted_func),
							fit_evaluation.report_TLI(fds[6], a_fitted_func, 2), "aprc")
			elif not math.isnan(d_error) and math.isnan(a_error):
				final_dp_fit = const.F_DP_FIT(fds[0], fds[1], fds[2], fds[3], fds[4], fds[5], fds[6], fds[7], d_yfit,
							d_popt, d_error, d_error_n, d_error_d, fit_evaluation.report_simple_snr(fds[6], d_fitted_func),
					        fit_evaluation.report_r_squared(fds[6], d_fitted_func, 3), fit_evaluation.report_pearson_corr(fds[6], d_fitted_func),
							fit_evaluation.report_TLI(fds[6], d_fitted_func, 3), "dprc")
			else:
				final_dp_fit = const.F_DP_FIT(fds[0], fds[1], fds[2], fds[3], fds[4], fds[5], fds[6], fds[7], [],
											  "None", d_error, d_error_n, d_error_d, "None", "None", "None", "None", "None")
			temp_fitteds.append(final_dp_fit)


	return temp_fitteds



'''
def run(self, input_file, expr_type, prefix):
	self._read_input(input_file, expr_type)
	self._run_normalization_byR(prefix)
	self._fit_both()
	self._print_f_dp_fit(prefix)
'''




def fit_one(n_rpkms, blacklist, aord):
	#TODO  What if we want to work with the ratios
	#TODO  What if we dont want to choose
	#TODO Make three list, first equation, second, the best
	#TODO Whats a cleaner way to do this? Panda library maybe?
	'''
	Fit based on both of the functions, keep the best one based on error rate
	:param n_rpkms: normalized rpkms are the input (Using Spike In Data)
	:param blacklist:
	:return:
	'''
	temp_fitteds =[]
	count = 0
	for fds in n_rpkms:
		count +=1
		if fds[0] not in blacklist:
			#print fds[0]+"\n"
			#a_fitted_func, a_yfit, a_popt, a_error, a_error_n, a_error_d = fit_aprc(const.A_INIT, fds[6], False, False)
			#a_dp_fit = const.DP_FIT(fds[0], fds[1], fds[2], fds[3], fds[4], fds[5], fds[6], fds[7], a_yfit,
			#						a_popt, a_error, a_error_n, a_error_d)

			#d_fitted_func, d_yfit, d_popt, d_error, d_error_n, d_error_d = fit_dprc(const.D_INIT, fds[6], False, False)
			#d_dp_fit = const.DP_FIT(fds[0], fds[1], fds[2], fds[3], fds[4], fds[5], fds[6], fds[7], d_yfit, d_popt, d_error,
			#				d_error_n, d_error_d)
			if aord == 'a' :
				a_fitted_func, a_yfit, a_popt, a_error, a_error_n, a_error_d = fit_aprc(const.A_INIT, fds[6], False,
																						False)
				a_dp_fit = const.DP_FIT(fds[0], fds[1], fds[2], fds[3], fds[4], fds[5], fds[6], fds[7], a_yfit,
										a_popt, a_error, a_error_n, a_error_d)
				if not math.isnan(a_error):
					final_dp_fit = const.F_DP_FIT(fds[0], fds[1], fds[2], fds[3], fds[4], fds[5], fds[6], fds[7], a_yfit,
							a_popt, a_error, a_error_n, a_error_d, fit_evaluation.report_simple_snr(fds[6], a_fitted_func),
					        fit_evaluation.report_r_squared(fds[6], a_fitted_func, 2), fit_evaluation.report_pearson_corr(fds[6], a_fitted_func),
							fit_evaluation.report_TLI(fds[6], a_fitted_func, 2), "aprc")
				else:
					final_dp_fit = const.F_DP_FIT(fds[0], fds[1], fds[2], fds[3], fds[4], fds[5], fds[6], fds[7], [],
												  [], a_error, a_error_n, a_error_d, "None", "None", "None", "None", "None")

			elif aord == 'd':
				d_fitted_func, d_yfit, d_popt, d_error, d_error_n, d_error_d = fit_dprc(const.D_INIT, fds[6], False,
																						False)
				d_dp_fit = const.DP_FIT(fds[0], fds[1], fds[2], fds[3], fds[4], fds[5], fds[6], fds[7], d_yfit, d_popt,
										d_error, d_error_n, d_error_d)
				if not math.isnan(d_error):
					final_dp_fit = const.F_DP_FIT(fds[0], fds[1], fds[2], fds[3], fds[4], fds[5], fds[6], fds[7], d_yfit,
							d_popt, d_error, d_error_n, d_error_d, fit_evaluation.report_simple_snr(fds[6], d_fitted_func),
					        fit_evaluation.report_r_squared(fds[6], d_fitted_func, 3), fit_evaluation.report_pearson_corr(fds[6], d_fitted_func),
							fit_evaluation.report_TLI(fds[6], d_fitted_func, 3), "dprc")
				else:
					final_dp_fit = const.F_DP_FIT(fds[0], fds[1], fds[2], fds[3], fds[4], fds[5], fds[6], fds[7], [],
											  [], d_error, d_error_n, d_error_d, "None", "None", "None", "None", "None")
			temp_fitteds.append(final_dp_fit)
		if count%20000 == 0:
			print str(count) + "   fitting is done"

	return temp_fitteds


def run_basic_fit(args):
	rcs = io_utils.read_input_t1(args.input)
	fit_type = args.fitting_function
	if fit_type!= 'best':
		fitted = fit_one(rcs,[],fit_type)
	else:
		fitted = fit_both(rcs, [])

	print "There are   " + str(len(fitted)) + "  instants fitted. "
	print "Simple filter is being used here to separate not fitted ones."

	after_fit_blacklist = filter.filter_after_fit(rcs, fitted)

	print "There are   " + str(len(after_fit_blacklist)) + "  instants  in the blacklist"
	fitted_filtered = filter.filter_by_blacklist(fitted, after_fit_blacklist)

	print "Finally, there are   " + str(len(fitted_filtered)) + " instants  remained and will be saved"
	io_utils.save_fitted(fitted_filtered, args.output)






