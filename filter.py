import const
from collections import defaultdict
import utils
import io_utils



def filter_by_thresh(rpkms, minrpkm, mintp, mindp):
	'''
	filter based on rpkm
	:param minrpkm: minimum rpkm for each time point to be counted strong
	:param mintp: minimum number of time points with minrpkm for a strong data point
	:param mindp: minimum number of exon/intron with enough strong point
	:return:
	'''
	print "Making a dictionary here, though not sure if it is really a good idea"
	rpkms_dict = utils.countdict_from_list_gbased(rpkms, False)
	print "Dictionary is generated. Was it a good idea?"
	low_rpkms_genes =[]
	for gene in rpkms_dict:
		# I'm counting the number of time points with good enough rpkms
		# for each exon or intron of this gene
		with_good_rpkm = []
		for p in rpkms_dict[gene]:
			with_good_rpkm.append(sum([1 if x > minrpkm else 0 for x in p]))
		min_dp = int(len(with_good_rpkm) * mindp)
		good_dp = sum([1 if x >= mintp else 0 for x in with_good_rpkm])
		if good_dp < min_dp:
			low_rpkms_genes.append(gene)
	return low_rpkms_genes


def filter_after_fit(ref_rpkms, rpkms):
	'''
	Put the gene in a blacklist if less than half of its exons are not fitted
	:param ref_rpkms: initial list to see how many exon in a list
	:param rpkms: list of F_DP_FITs after fitting
	:return:
	'''
	ref_rpkms_dict = utils.countdict_from_list_gbased(ref_rpkms, False)
	rpkms_dict = defaultdict(list)
	for fds in rpkms:
		if fds[-1] != "None":
			rpkms_dict[fds[0]].append(fds[6])
	genes_with_bad_fit = []
	for gene in rpkms_dict:
		if len(rpkms_dict[gene]) < 0.5* (len(ref_rpkms_dict[gene])):
			genes_with_bad_fit.append(gene)

	return genes_with_bad_fit

def filter_by_blacklist(rpkms, blacklist):
	temp_filtered = []
	for fds in rpkms:
		if fds[0] not in blacklist and fds[-1]!="None":
			temp_filtered.append(fds)

	return temp_filtered


def filter_by_fitting_score(fitted, ftype, thresh):
	#TODO make sure about the scores
	'''
	this is where I want to put the threshold.
	:param fitted:
	:return:
	'''
	gname = fitted[0][0]
	bads = 0
	templist = []
	blacklist = []

	for dp in fitted:
		if dp[0] == gname:
			templist.append(dp)
		else:
			#if bads > 0.5* len(templist):
			#	blacklist.append(gname)


			if bads > 0:
				blacklist.append(gname)
			gname = dp[0]
			bads = 0
			templist = [dp]

		scores = [dp[10], dp[13], dp[14], dp[15], dp[16]]
		mse = scores[0]
		snr = scores[1]
		r2 = scores[2][0]
		r2adj = scores[2][1]
		pearson = scores[3]
		tli = scores[4]

		if ftype == 'snr':
			if snr < thresh :
				bads+=1
		elif ftype == 'r2':
			if r2 < thresh:
				bads +=1
		elif ftype == 'r2adj':
			if r2adj < thresh:
				bads +=1

		elif ftype == 'pearson':
			if pearson < thresh:
				bads +=1

		elif ftype == 'tli':
			if tli < thresh:
				bads +=1
		elif ftype == 'mse':
			if mse > thresh:
				bads +=1

	#if bads > 0.5 * len(templist):
	#	blacklist.append(gname)

	if bads > 0:
		blacklist.append(gname)

	return blacklist


def run_filter_after_fit(args):
	fitted = io_utils.read_input_t2(args.input)
	filtertype = args.filter_type
	threshold = args.threshold
	blacklist = filter_by_fitting_score(fitted, filtertype, threshold)

	fitted_filtered = filter_by_blacklist(fitted, blacklist)
	io_utils.save_fitted(fitted_filtered, args.output)


def run_filter_before_fit(args):
	rpkms = io_utils.read_input_t1(args.input, 0)
	blacklist = filter_by_thresh(rpkms, 0.1, 3, 0.5)

	rpkms_filtered = filter_by_blacklist(rpkms, blacklist)

	io_utils.save_ratios_from_rpkms(rpkms_filtered, args.output)
	print "Just filtered the low expressed ones."
	print "the output is saved in:  " + str(args.output) + "\n"
	print "===================================================="